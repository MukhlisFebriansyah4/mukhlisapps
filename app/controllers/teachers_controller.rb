class TeachersController < ApplicationController
    def new #untuk menampilkan form data baru
    @teacher = Teacher.new
    end
    
    def create #untuk memproses data baru yamg dimasukan di form new
    teacher = Teacher.new(resource_params)
    teacher.save
    flash[:notice] = 'Book has been created'
    redirect_to teachers_path
    end
    
    def edit #menampilkan data yang sudah disimpan di edit
    @teacher = Teacher.find(params[:id])
    end
    
    def update #melakukan proses ketika user mengedit data
    @teacher = Teacher.find(params[:id])
    @teacher.update(resource_params)
    flash[:notice] = 'Book has been update'
    redirect_to teachers_path(@teacher)
    end
    
    def destroy #untuk menghapus data
    @teacher = Teacher.find(params[:id])
    @teacher.destroy
    flash[:notice] = 'Book has been slain'
    redirect_to teachers_path
    end
    
    def index #menampilkan seluruh data yang ada di database
    @teachers = Teacher.all
    end
    
    def show #menampilkan sebuah data secara detail
    id = params[:id]
    @teacher = Teacher.find(id)
    end
    
    private
    def resource_params
    params.require(:teacher).permit( :NIK, :name, :age,:kelas, :mapel)
    end
end
