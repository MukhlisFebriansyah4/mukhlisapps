Rails.application.routes.draw do
  resources :users, only: [:new, :create]

  get 'login', to: 'sessions#new'

  post 'login', to: 'sessions#create'

  get 'welcome', to: 'sessions#welcome'

  get 'authorized', to: 'sessions#page_requires_login'
  
  resources :reports
  resources :teachers
  resources :exams
  resources :students
  root 'sessions#new'
  get 'users/index'
  get 'beranda/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
