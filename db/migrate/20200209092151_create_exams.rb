class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :title
      t.string :mapel
      t.integer :duration
      t.integer :nilai
      t.string :status
      t.integer :id_student

      t.timestamps
    end
  end
  def down
    drop_table :exams
  end
end
