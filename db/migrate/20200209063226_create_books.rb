class CreateBooks < ActiveRecord::Migration[6.0]
  def up
    create_table :books do |t|
    t.string :title, default: 'belum ada nama', limit:50
    t.integer :page, default: 0
    t.integer :price, null: false
    t.text :description
    
    t.timestamps
    end
    end
  def down
      drop_table:books
  end
end
