class CreateReports < ActiveRecord::Migration[6.0]
  def up
    create_table :reports do |t|
      t.string :title
      t.string :hasil
      t.string :mapel
      t.integer :teacher_id
      t.integer :student_id
      t.string :date

      t.timestamps
    end
  end
  def down
    drop_table :reports
  end
end
